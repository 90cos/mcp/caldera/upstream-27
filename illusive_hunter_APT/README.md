# Advanced Persistent Threat 

### Capabilities:
- Ransomeware (LockerGoga)
- Crypto Mining (XMRig)
- Multiple Platform Payload/Malware Delivery

### Notes for integration:
- Malware Needs to be dropped in /stockpile/data/payloads/ folder for SandCat to do work.

- LockerGoga.exe should be named goga.exe or `e22f3eb8-d794-4177-870a-6357d6eddd8f.yml` ability will need to be modified to match filename.

- XMRig requires to binaries to execute properly. This ability is stored in `437063b4-d05a-414e-8331-e4f9027e1c75`:
    - xmrigDaemon.exe
    - xmrigMiner.exe



## UUID Mapping:
#### Adversaries
- CyberCrime (CryptoMining) : `179cdf15-6224-4abc-9b24-17480c3d050d`
- Operation Midnight : `41a21170-02b1-4bc8-8d19-aae18c25634f`
- Brick The Box : `fd4f48cd-a52a-4b58-8c0a-0609bea3c4ad`
#### Abilities
- Crypto Mining Operations : `437063b4-d05a-414e-8331-e4f9027e1c75`
- LockerGoga Ransomeware : `e22f3eb8-d794-4177-870a-6357d6eddd8f`
- Grab All Known IP Addresses : `8978ef0f-4b56-4589-b711-e10ebdbcc3a7`
- Locate files that have been encrypted by Ransomware: `de320707-4ee5-428c-b641-bacd087ec3b0`
- Get Average CPU Load of the Processor: `f54d23f8-d449-48ed-8e27-ad4e47c43fe2`
- Hide Ransomeware (Disable Windows Defender): `ba9c1c9e-3b43-45db-855d-73da0c7fd5ec`
- Modify Wallpaper on target to strike fear: `c9f06cc2-1a01-433a-a113-8f892ded2eac`
 


> All UUID's were created using the python3 library `uuid`

