from plugins.mcp.app.parsers.jsonencode import Parser as BasicParser


class Parser(BasicParser):
    """
    ability 90c2efaa-8205-480d-8bb6-61d90dbaf81b (find sensitive files)
     hard codes the top 5 results as facts.

     this module accepts any number as the upload but only makes facts for the first 5

     probably should overwrite self.line to reduce the list copies
    """
    @staticmethod
    def line(blob):
        """
        Split a blob by line
        :param blob:
        :return:
        """
        # -----------------------------------------v <- lol
        return [x for x in blob.split('\n') if x][-5:]

    def parse(self, blob):
        newblob = "\n".join(self.line(blob))
        relationships = super().parse(newblob)
        return relationships
