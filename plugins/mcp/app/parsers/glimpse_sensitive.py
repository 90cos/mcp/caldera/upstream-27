from plugins.mcp.app.parsers.jsonencode import Parser as BasicParser


class Parser(BasicParser):
    """
    strips glimpse headers and adds the first 5 results as facts
    """

    def parse(self, blob):
        newblob = "\n".join(self.line(blob)[4:-2][-5:])  # skip the first 3 lines and the last line then only grab 5
        relationships = super().parse(newblob)
        return relationships
