from app.objects.c_relationship import Relationship
from plugins.stockpile.app.parsers.basic import Parser as BasicParser

import json


class Parser(BasicParser):

    def parse(self, blob):
        relationships = []
        for match in self.line(blob):
            match = match.strip()
            if match:
                match = json.dumps(match).strip('"')  # plan to embed this in a longer json string no quote needed
                for mp in self.mappers:
                    source = self.set_value(mp.source, match, self.used_facts)
                    target = self.set_value(mp.target, match, self.used_facts)
                    relationships.append(
                        Relationship(source=(mp.source, source),
                                     edge=mp.edge,
                                     target=(mp.target, target))
                    )
        return relationships
