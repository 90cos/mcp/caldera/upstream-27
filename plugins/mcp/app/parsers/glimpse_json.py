from plugins.stockpile.app.parsers.json import Parser as BasicParser


class Parser(BasicParser):

    def parse(self, blob):
        newblob = "\n".join(self.line(blob)[4:-2])  # glimpse has wrapping around output
        relationships = super().parse(newblob)
        return relationships
