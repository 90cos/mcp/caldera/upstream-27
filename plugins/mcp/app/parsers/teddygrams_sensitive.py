from plugins.mcp.app.parsers.jsonencode import Parser as BasicParser


class Parser(BasicParser):

    def parse(self, blob):
        relationships = []
        newblob = "\n".join(self.line(blob)[1:])  # skip the first line
        relationships = super().parse(newblob)
        return relationships
