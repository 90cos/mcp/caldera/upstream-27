from plugins.mcp.app.parsers.jsonencode import Parser as BasicParser


class Parser(BasicParser):

    def parse(self, blob):
        newblob = blob.replace("\\", "\\\\")
        relationships = super().parse(newblob)
        return relationships
