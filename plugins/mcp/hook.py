import logging
from time import sleep
name = 'MCP'
description = 'Caldera / MCP Interoperability'
address = None


async def enable(services):
    logging.debug("Caldera / MCP is enabled by plugins/mcp/hook.py")


    loaded = await services.get('data_svc').load_data()


