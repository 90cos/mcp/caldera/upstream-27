function refresh_file_listing(){
    $( "#pcpdownloader" ).load("http://127.0.0.1:8888/plugin/brain/gui" );
}

function upload_to_brain(){

}

function populate_file_listing(){
    var file_list_dom = $(".upload_file_list");
    var file_refresh = $("#upload_file_refresh");
    file_refresh.addClass("fa-spin");
    file_list_dom.empty();
    var counter_int;
    $.ajax({
        type: "GET",
        url: "/plugin/brain/gui/",
        datatype: 'json',
        success: function(data) {
            var dan = data; //http://127.0.0.1:8443/gui/img/x.png
            counter_int = 0;
            for (var idx in data){
                var filename = data[idx];
                dom_filename_map[counter_int] = filename;
                counter_int++;
                add_file_to_dropzone_list(filename);
            }
            file_refresh.removeClass("fa-spin");
            $("#upload_files_need_refreshed").hide();
        }
    });
}

$(document).ready(function() {
    populate_file_listing();
    $("#upload_file_refresh").click(populate_file_listing);

});