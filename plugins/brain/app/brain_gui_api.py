from aiohttp_jinja2 import template
from aiohttp import web
from brain import connect, r
from brain.binary import get, put, delete, list_dir
from brain.static import RBF, RBM
from brain.binary import CONTENTTYPE_FIELD, PRIMARY_FIELD, CONTENT_FIELD
from app.service.auth_svc import check_authorization
from app.utility.base_world import BaseWorld


class BrainGuiApi(BaseWorld):

    def __init__(self, services):
        self.auth_svc = services.get('auth_svc')

    @check_authorization
    @template('brain-list.html')
    async def splash(self, request):

        files = []
        db_conn = connect()
        filenames = list_dir()  # indexed query
        for filename in filenames:  # carefully pluck out only the needed field from indexed get
            contenttype = RBF.get(
                filename
            ).pluck(
                CONTENTTYPE_FIELD
            ).run(
                db_conn
            )
            files.append({
                PRIMARY_FIELD: filename,
                CONTENTTYPE_FIELD: contenttype[CONTENTTYPE_FIELD]
            })
        return dict(files=files)

    @check_authorization
    async def getfile(self, request):
        fileid = request.match_info['fileid']
        fileobj = get(fileid)
        return web.Response(
            body=fileobj[CONTENT_FIELD]
        )



