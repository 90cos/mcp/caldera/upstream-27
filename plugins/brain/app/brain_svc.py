import glob

from aiohttp_jinja2 import template, web
from brain.binary import delete, put_buffer
from app.service.auth_svc import check_authorization
from app.utility.base_service import BaseService


class BrainService(BaseService):

    def __init__(self, services):
        self.auth_svc = services.get('auth_svc')
        self.file_svc = services.get('file_svc')
        self.data_svc = services.get('data_svc')
        self.contact_svc = services.get('contact_svc')
        self.log = self.add_service('brain_svc', self)

    @check_authorization
    async def uplfile(self, request):
        post = await request.post()
        content = post.get("file")
        blob = content.file.read()
        result = put_buffer(post['file'].filename, blob)
        return web.json_response(result)

    @check_authorization
    async def delfile(self, request):
        fileid = request.match_info['fileid']
        return web.json_response(delete(fileid))

