from aiohttp_jinja2 import web, template
from plugins.brain.app.brain_svc import BrainService
from plugins.brain.app.brain_gui_api import BrainGuiApi
from app.utility.base_world import BaseWorld

name = 'Brain'
description = 'Sample MCP Interop to show files in Brain'
address = "/plugin/brain/gui"
access = BaseWorld.Access.RED


async def enable(services):
    brain_svc = BrainService(services)
    brain_gui = BrainGuiApi(services=services)
    # Added like sandcat
    app = services.get('app_svc').application
    app.router.add_static('/brain', 'plugins/brain/static', append_version=True)
    app.router.add_route('GET', '/plugin/brain/gui', brain_gui.splash)
    app.router.add_route('GET', '/plugin/brain/get/{fileid}', brain_gui.getfile)
    app.router.add_route('POST', '/plugin/brain/upload', brain_svc.uplfile)
    app.router.add_route('GET', '/plugin/brain/delete/{fileid}', brain_svc.delfile)
    #services.get('app_svc').application.router.add_route("GET", '/plugin/brain/gui', brain_gui.splash)


