"""
includes some packers.

For now, each pack_* function is generally self contained.
This will lead to a lot of duplicate code.
Need to sort out the integration mechanics with caldera first.

"""


import os
import pexpect
from random import choice
from tempfile import TemporaryDirectory
from multidict import CIMultiDict, CIMultiDictProxy
from app.utility.base_service import BaseService


class PackService(BaseService):

    def __init__(self, services):
        self.file_svc = services.get('file_svc')
        self.data_svc = services.get('data_svc')
        self.contact_svc = services.get('contact_svc')
        self.app_svc = services.get('app_svc')
        self.log = self.create_logger('pack_svc')
        self.pack_dir = os.path.relpath(os.path.join('plugins', 'packer'))
        self.log.debug("packer initialized")

    async def pack_upx(self, headers):
        """
        TODO: Deal with recursion problems if they call
        file: upx and source: upx.  Pretend people are lawful neutral for now.
        :param headers:
        :return:
        """
        name, platform, source = headers.get('file'), headers.get('platform'), headers.get('source')
        # sort out sane defaults
        lzma = "--lzma" if headers.get("lzma") else ""
        try:
            compression_number = str(int(headers.get("compression"))) if 1 <= int(headers.get("compression")) <= 9 else "9"
        except (ValueError, TypeError):
            compression_number = "9"
        compression = f'-{compression_number}'

        self.log.critical(f"UPX module activated: {source} / {platform}")

        recursive_headers = self._clone_headers(headers)
        file_path, contents, display_name = await self.file_svc.get_file(recursive_headers)
        file_compressed = None
        # should be doing this using self.file_svc?
        # But I don't want this intermediate file written where it could be scooped up?
        try:
            tempdir = TemporaryDirectory()
            filename = f"{tempdir.name}/{source}"
            self.log.critical(f"UPX filename: {filename}")
            with open(filename, "wb") as f:
                f.write(contents)

            pack_cmd = f"upx {lzma} {compression} {filename}"
            self.log.critical(f"UPX cmd: {pack_cmd}")

            output = pexpect.run(pack_cmd)
            if b"CantPackException" in output:
                raise ValueError(f"Cannot UPX pack {source}")

            with open(filename, "rb") as f:
                file_compressed = f.read()
                self.log.critical("UPX filename size : {}".format(len(file_compressed)))
        finally:
            tempdir.cleanup()

        output_filename = f"{source}-{platform}-upx"
        await self.file_svc.save_file(
            output_filename,
            file_compressed,
            "plugins/packer/payloads/",
        )
        self.log.critical("UPX filename size : {}".format(len(file_compressed)))
        return output_filename, display_name

    async def pack_woody(self, headers):
        """
        TODO: Deal with recursion problems if they call
        file: woody and source: woody.  Pretend people are lawful neutral for now.
        :param headers:
        :return:
        """
        name, platform, source = headers.get('file'), headers.get('platform'), headers.get('source')
        # sort out sane defaults
        self.log.debug(f"Woody module activated: {source} / {platform}")
        recursive_headers = self._clone_headers(headers)
        file_path, contents, display_name = await self.file_svc.get_file(recursive_headers)
        print("got the file")
        file_compressed = None
        # should be doing this using self.file_svc?
        # But I don't want this intermediate file written where it could be scooped up?
        try:
            tempdir = TemporaryDirectory()
            filename = f"{tempdir.name}/{source}"
            self.log.critical(f"Woody filename: {filename}")
            with open(filename, "wb") as f:
                f.write(contents)

            pack_cmd = f"woody_woodpacker  {filename}"
            self.log.debug(f"woody cmd: {pack_cmd}")

            output = pexpect.run(pack_cmd,
                                 cwd=tempdir.name)  # packer writes the file "woody" to CWD
            if b"PE 64" not in output:
                self.log.critical(f"Woody: not a PEfile {source}")
                raise ValueError(f"Cannot woody pack {source}")
            woody_filename = f"{tempdir.name}/woody"
            with open(woody_filename, "rb") as f:
                file_compressed = f.read()
                self.log.debug("woody filename size : {}".format(len(file_compressed)))
        finally:
            tempdir.cleanup()

        output_filename = f"{source}-{platform}-woody"
        await self.file_svc.save_file(
            output_filename,
            file_compressed,
            "plugins/packer/payloads/",
        )
        self.log.critical("woody filename size : {}".format(len(file_compressed)))
        return output_filename, display_name

    async def pack_amber(self, headers):
        """
        TODO: Deal with recursion problems if they call
        file: woody and source: woody.  Pretend people are lawful neutral for now.
        :param headers:
        :return:
        """
        self.log.debug("ENTERING amber")
        name, platform, source = headers.get('file'), headers.get('platform'), headers.get('source')
        # sort out sane defaults
        self.log.debug(f"AMBER module activated: {source} / {platform}")
        try:
            key_number = str(int(headers.get("keysize"))) if 8 <= int(headers.get("keysize")) <= 255 else "8"
        except (ValueError, TypeError):
            key_number = "8"
        keysize = f"-k {key_number}"
        reflective = "-r" if headers.get('reflective') else ""
        anti_analysis = "-a" if headers.get('anti-analysis') else ""
        ignore_integrity = "-ignore-integrity" if headers.get('ignore-integrity') else ""
        iat = "-i" if headers.get("use-iat") else ""

        recursive_headers = self._clone_headers(headers)
        file_path, contents, display_name = await self.file_svc.get_file(recursive_headers)
        file_compressed = None
        # should be doing this using self.file_svc?
        # But I don't want this intermediate file written where it could be scooped up?
        try:
            tempdir = TemporaryDirectory()
            filename = f"{tempdir.name}/{source}"
            self.log.critical(f"Amber source filename: {filename}")
            with open(filename, "wb") as f:
                f.write(contents)

            pack_cmd = f"amber {keysize}  {reflective} {anti_analysis} {ignore_integrity} {iat} {filename}"
            self.log.debug(f"amber cmd: {pack_cmd}")

            output = pexpect.run(pack_cmd,
                                 cwd=tempdir.name)
            if b"File successfully packed" not in output:
                self.log.critical(f"Amber: could not be packed {source}")
                raise ValueError(f"Cannot amber pack {source}")
            amber_output_name = filename
            with open(amber_output_name, "rb") as f:
                file_compressed = f.read()
                self.log.debug("Amber filename size : {}".format(len(file_compressed)))
        finally:
            tempdir.cleanup()

        output_filename = f"{source}-{platform}-amber"
        await self.file_svc.save_file(
            output_filename,
            file_compressed,
            "plugins/packer/payloads/",
        )
        self.log.critical("amber filename size : {}".format(len(file_compressed)))
        return output_filename, display_name

    def _clone_headers(self, headers):
        """
        clone the header dict with everything but the file key, then add the file key from the source value
        :param headers:
        :return:
        """
        cloned = [
            item for item in headers.items() if item[0] != "file"
        ]
        cloned += [
            ("file", headers["source"])
        ]
        return CIMultiDictProxy(CIMultiDict(cloned))
