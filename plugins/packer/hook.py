from app.utility.base_world import BaseWorld
from plugins.packer.app.pack_gui_api import PackGuiApi
from plugins.packer.app.pack_svc import PackService

name = 'Packer'
description = 'pack blobs on the way out the door'
address = '/plugin/packer/gui'
access = BaseWorld.Access.RED


async def enable(services):
    app = services.get('app_svc').application
    file_svc = services.get('file_svc')
    pack_svc = PackService(services)
    await file_svc.add_special_payload('upx', pack_svc.pack_upx)
    await file_svc.add_special_payload('woody', pack_svc.pack_woody)
    await file_svc.add_special_payload('amber', pack_svc.pack_amber)
    pack_gui_api = PackGuiApi(services=services)
    app.router.add_static('/packer', 'plugins/packer/static', append_version=True)
    app.router.add_route('GET', '/plugin/packer/gui', pack_gui_api.splash)
