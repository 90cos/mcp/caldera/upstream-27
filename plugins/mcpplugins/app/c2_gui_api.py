from aiohttp_jinja2 import template
from time import time
from aiohttp import web
from app.service.auth_svc import check_authorization
from app.utility.base_world import BaseWorld

from brain.controller.plugins import get_plugins, create_plugin, find_plugin, \
    stop as brain_stop, restart as brain_restart
from brain.controller.plugins import DESIRE_ACTIVE
from brain import connect
from brain.static import RPP

class C2GuiApi(BaseWorld):

    def __init__(self, services):
        self.auth_svc = services.get('auth_svc')

    @check_authorization
    @template('mcpc2.html')
    async def splash(self, request):
        plugins = get_plugins()
        interfaces = list(RPP.run(connect()))
        return dict(plugins=plugins, interfaces=interfaces)

    @check_authorization
    async def update(self, request):
        requested_plugin = await request.json()
        result = self._clean_and_submit_to_brain(requested_plugin)
        return web.json_response(result)

    @check_authorization
    async def restart(self, request):
        plugin_id = request.match_info['pluginid']
        return web.json_response(brain_restart(plugin_id))

    @check_authorization
    async def stop(self, request):
        plugin_id = request.match_info['pluginid']
        return web.json_response(brain_stop(plugin_id))

    @staticmethod
    def _clean_and_submit_to_brain(plugin):
        """
        fix ports and environment to list
        :param plugin:
        :return:
        """
        from copy import deepcopy

        # cleanup the user stuff first
        del plugin['id']
        plugin["ExternalPorts"] = [
            x.strip()
            for x in
            plugin["ExternalPorts"].split(",")
            if x
        ]
        plugin["InternalPorts"] = deepcopy(plugin["ExternalPorts"])
        plugin["Environment"] = [
            x.strip()
            for x in
            plugin["Environment"].split(",")
            if x
        ]

        # get the template from the brain
        source_plugins = find_plugin(plugin["Name"])
        for brain_template in source_plugins:
            if brain_template['ServiceName'] == "" and "ImageName" in brain_template:
                del brain_template['id']
                break

        # fill out the template

        timestamp = str(int(time()))
        brain_template["DesiredState"] = DESIRE_ACTIVE
        brain_template["ServiceName"] = f"{plugin['Name']}-{timestamp}"
        brain_template["ServiceID"] = "NEW"
        brain_template["Environment"] = plugin["Environment"]
        brain_template["InternalPorts"] = plugin["InternalPorts"]
        brain_template["ExternalPorts"] = plugin["ExternalPorts"]
        brain_template["Interface"] = plugin["Interface"]
        return create_plugin(brain_template)
