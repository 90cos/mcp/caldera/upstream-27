from app.utility.base_world import BaseWorld
from plugins.mcpplugins.app.c2_gui_api import C2GuiApi
from plugins.mcpplugins.app.c2_svc import C2Service

name = 'MCP Plugins'
description = 'Provision C2s for MCP'
splash_address = '/plugin/mcpplugins/gui'
update_address = '/plugin/mcpplugins/update'
restart_address = '/plugin/mcpplugins/restart/{pluginid}'
stop_address = '/plugin/mcpplugins/stop/{pluginid}'

# required by caldera core
address = splash_address
access = BaseWorld.Access.RED


async def enable(services):
    app = services.get('app_svc').application
    file_svc = services.get('file_svc')
    mcpplugin_svc = C2Service(services)
    mcpplugin_gui_api = C2GuiApi(services=services)
    app.router.add_static('/mcpplugins', 'plugins/mcpplugins/static', append_version=True)
    app.router.add_route('GET', splash_address, mcpplugin_gui_api.splash)
    app.router.add_route('POST', update_address, mcpplugin_gui_api.update)
    app.router.add_route('GET', restart_address, mcpplugin_gui_api.restart)
    app.router.add_route('GET', stop_address, mcpplugin_gui_api.stop)
