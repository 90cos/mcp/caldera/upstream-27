from app.utility.base_world import BaseWorld
from plugins.mcptargets.app.targets_gui_api import TargetsGuiApi

name = 'MCPTargets'
description = 'Active MCP Targets information'
splash_address = '/plugin/mcptargets/gui'

# required by caldera core
address = splash_address
access = BaseWorld.Access.RED


async def enable(services):
    app = services.get('app_svc').application
    mcptargets_gui_api = TargetsGuiApi(services=services)
    app.router.add_static('/mcptargets', 'plugins/mcptargets/static', append_version=True)
    app.router.add_route('GET', splash_address, mcptargets_gui_api.splash)


    