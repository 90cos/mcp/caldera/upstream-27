from aiohttp_jinja2 import template
from time import time
from datetime import datetime
from functools import reduce
from aiohttp import web

from app.utility.base_world import BaseWorld
from brain import connect
from brain.static import RBT


class TargetsGuiApi(BaseWorld):

    def __init__(self, services):
        self.auth_svc = services.get('auth_svc')

    @template('mcptargets.html')
    async def splash(self, request):
        targets = list(RBT.run(connect()))
        targets = self.clean_target_data(targets)
        return dict(targets=targets)

    @staticmethod
    def clean_target_data(targets):
        # Easy style to grab datafields from BRAIN database and translate into easy form to use
        extractHeaders = {
            "CheckIn" : "Optional.Common.Checkin",
            "Hostname" : "Optional.Specific.telemetry.name",
            "ExternalIP" : "Location",
            "Port" : "Port",
            "Admin" : "Optional.Common.Admin",
            "User" : "Optional.Common.User",
            "InternalIP" : "Optional.Specific.InternalLocation",
            "Plugin" : "PluginName"
        }
        allTargets = []
        for t in targets:
            singleTarget = {'headers' : {}, 'raw' : t.copy()} # Copy all headers info and raw data
            for h in extractHeaders.items(): # Check each extractHeaders and parse data from it if it exists from the BRAIN
                dictLocation = h[1].split('.') # Split extractHeaders 2nd field by '.'
                # Single line of code to search through nested json to find specific key values
                singleTarget['headers'][h[0]] = reduce(lambda c, k: c.get(k, {}), dictLocation, t)
            if(singleTarget['headers']['CheckIn']):
                # Covert datetime from epoch to readable format for display
                singleTarget['headers']['CheckIn'] = str(datetime.fromtimestamp(singleTarget['headers']['CheckIn']).strftime("%d %b, %Y  %H:%M:%S"))
            allTargets.append(singleTarget)
        return allTargets


