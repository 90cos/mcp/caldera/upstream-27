FROM alpine:3.10

WORKDIR /usr/src/app
ADD . /usr/src/app

# remove dev venv (if exists)
RUN rm -rf venv


RUN apk update && apk upgrade && \
    apk add python3 go git python3-dev py3-cffi openssl-dev haproxy musl-dev libffi-dev dumb-init && \
    apk add py3-cryptography  && \
    pip3 install -r requirements.txt && \
    pip3 install --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain  && \
    pip3 install -r requirements-new.txt


ENTRYPOINT  ["/usr/bin/dumb-init", "--"]
CMD ["python3", "server.py"]
